[![gitlab pages deployed page link](https://img.shields.io/badge/Gitlab-Pages-orange?logo=gitlab)](https://nicolalandro.gitlab.io/study-pannellum/)

# Study Pannellum
This codebase is a study of pannellum lib. It also contain a study for creating images using Hugin.

## How to develop

```
# docker-compose up
docker compose up
# open browser at localhost:8000
```

##  References
* Docker and Docker compose: to develop in a platform indipendent way
* html, css, js: languages for web page creations
* [Pannellum](https://pannellum.org/)
  * [Used example](https://github.com/mpetroff/pannellum/blob/master/examples/example-tour.json) 
* HDRI/360 images: the kind of images that we want to show with pannellum
  * [Hugin](https://appimage.github.io/hugin/): software for merging image and create panorama and 360
    * [360 tutorial](https://www.youtube.com/watch?v=TerkdecfryQ)
